FROM continuumio/miniconda3:4.10.3p0
RUN apt-get update
RUN conda config --add channels bioconda --add channels conda-forge --add channels cruizperez
RUN conda create -n microbeannotator -y python=3.7 pip microbeannotator=2.0.5
RUN echo 'alias ll="ls -all"' >> ~/.bashrc
RUN echo 'conda activate microbeannotator' >> ~/.bashrc
RUN /bin/bash -c 'source ~/.bashrc && pip install hmmer==0.1.0'
